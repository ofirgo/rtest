library(ggplot2)
library(rpart)
library(rpart.plot)
library(caTools)

setwd("~/R scripts")
churn.raw <- read.csv('churn.csv')
churn.df.prepared <-churn.raw
str(churn.df)

summary(churn.df.prepared)

any(is.na(churn.df.prepared))

#TotalCharges    
make.average<-function(x,vec){
  if(is.na(x)){
    return(mean(vec,na.rm=TRUE))
  }
  return(x)
}

churn.df.prepared$TotalCharges<-sapply(churn.df.prepared$TotalCharges,make.average,vec=churn.df.prepared$TotalCharges)

churn.df.prepared$customerID<-NULL
churn.df.prepared$SeniorCitizen<-as.factor(churn.df$SeniorCitizen)

#Q2-a
ggplot(churn.df.prepared, aes(tenure,fill=Churn))+geom_bar()
ggplot(churn.df.prepared, aes(tenure,fill=Churn))+geom_bar(position = 'fill')

coercx <- function(x, by){
  if(x>=by) return(x)
  return(by)
}
churn.df.prepared$tenure <- sapply(churn.df.prepared$tenure,coercx,by=1)

#Q2-b
ggplot(churn.df.prepared,aes(gender,fill=Churn))+geom_bar(position = 'fill')

#Q2-c
ggplot(churn.df.prepared,aes(Contract,fill=Churn))+geom_bar(position = 'fill')

#Q2-d
str(churn.df.prepared)

cf<-table(churn.df.prepared$PaperlessBilling,churn.df.prepared$Churn)
precision<-cf[2,2]/(cf[1,2]+cf[2,2])
#precision<-cf['Yes','Yes']/(cf['No','Yes']+cf['Yes','Yes'])
recall<-cf[2,2]/(cf[2,1]+cf[2,2])

#Q3 -a
churn.df<-churn.df.prepared
str(churn.df)
filter <- sample.split(churn.df$Churn, SplitRatio = 0.7)
churn.df.train <- subset(churn.df, filter == T)
churn.df.test <- subset(churn.df, filter == F)

model.dt <- rpart(Churn ~ TechSupport+tenure+Contract, churn.df.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

prediction<-predict(model.dt,churn.df.test)
prob.yes<-prediction[,'Yes']

actual<-churn.df.test$Churn

confusion.matrix <- table(prob.yes > 0.5,actual)

precision <- confusion.matrix[1,1]/(confusion.matrix[1,1] + confusion.matrix[1,2])
recall <- confusion.matrix[1,1]/(confusion.matrix[1,1] + confusion.matrix[2,1])

#number.churn <- dim(churn.df[churn.df$Churn=='Yes',])[1]
#base_level <- number.churn/dim(churn.df)[1]

#Q4 -a
model<-glm(Churn~.,family = binomial(link = 'logit'),data=churn.df.train)
summary(model)

#Q4 -b
prediction.glm<-predict(model,churn.df.test,type='response')

cf<-table(churn.df.test$Churn,prediction.glm>0.5)

precision.glm<-cf[2,2]/(cf[1,2]+cf[2,2])
recall.glm<-cf[2,2]/(cf[2,1]+cf[2,2])

total_errors <- (cf[1,2]+cf[2,1])/dim(churn.df.test)[1]

#Q4 -c
#ROC curve 

library(pROC)


rocCurveDT <- roc(churn.df.test$Churn,prob.yes, direction = ">", levels = c("Yes","No"))
rocCurveRL <- roc(churn.df.test$Churn,prediction.glm, direction = ">", levels = c("Yes","No"))

#plot the chart 
plot(rocCurveDT, col = 'red',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveRL, col = 'blue',main = "ROC Chart")

auc(rocCurveDT)
auc(rocCurveRL)
