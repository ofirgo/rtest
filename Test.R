#Q1 -a

setwd("~/R scripts")
car.df.raw <- read.csv('carInsurance_train.csv')
car.prepared<-car.df.raw

str(car.prepared)
#Q1 -b
#create new varaible from callstart and callend - callduration
library(lubridate)

car.prepared$CallStart<-as.character(car.prepared$CallStart)
car.prepared$CallEnd<-as.character(car.prepared$CallEnd)

#call.duration<-car.prepared$CallEnd-car.prepared$CallStart
Sys.setlocale("LC_TIME","English")

hours.start<-substr(car.prepared$CallStart,1,5)
minute.start<-as.integer(substr(hours.start,4,5))
car.prepared$minute.start<-minute.start

hours.end<-substr(car.prepared$CallEnd,1,5)
minute.end<-as.integer(substr(hours.end,4,5))
car.prepared$minute.end<-minute.end


times<-minute.end-minute.start


make.duration<-function(x){
  if(x<0){x=60+x}
  if(x==0){x=60}
  return(x)
}

call.duration<-sapply(times,make.duration)
car.prepared$call.duration<-call.duration

#Q1 -c

car.df <- car.prepared[,c("Job","Marital","Balance","HHInsurance","CarLoan","NoOfContacts","DaysPassed","Outcome","CarInsurance","call.duration")]
car<-as.data.frame(car.df)

#Q2
str(car)
summary(car)

#Balance
table(car$Balance)
ggplot(car,aes(Balance))+geom_histogram(binwidth =1000)

#HHInsurance
car$HHInsurance<-as.factor(car$HHInsurance)

#CarLoan
car$CarLoan<-as.factor(car$CarLoan) 

#Outcome
table(car$Outcome)

fix.outcome<-function(outcome){
  if(is.na(outcome))
    return('Unknown')
  return(outcome)
}

car$Outcome<-as.character(car$Outcome)
car$Outcome<-sapply(car$Outcome,fix.outcome)
car$Outcome<-as.factor(car$Outcome)

#Job
fix.job<-function(job){
  if(is.na(job))
    return('Unknown')
  return(job)
}

car$Job<-as.character(car$Job)
car$Job<-sapply(car$Job,fix.job)
car$Job<-as.factor(car$Job)
table(car$Job)

#Q3 -a
library(rpart)
library(rpart.plot)
library(caTools)

#split train_test
filter <- sample.split(car$CarInsurance, SplitRatio = 0.7)
car.train <- subset(car, filter == T)
car.test <- subset(car, filter == F)

dim(car.train)
dim(car.test)

#model DT
model.dt <- rpart(CarInsurance ~ ., car.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

prediction.dt <- predict(model.dt,car.test)
actual.dt <- car.test$CarInsurance

#Q3 -c
library(randomForest)
model.rf <- randomForest(CarInsurance ~ ., data = car.train, importance = TRUE)

prediction.rf <- predict(model.rf,car.test)
actual.rf <- car.test$CarInsurance
prediction<-prediction.rf > 0.6
confusion.matrix.rf <- table(prediction,actual.rf)

precision<-confusion.matrix.rf[2,2]/(confusion.matrix.rf[2,1]+confusion.matrix.rf[2,2])
recall<-confusion.matrix.rf[2,2]/(confusion.matrix.rf[1,2]+confusion.matrix.rf[2,2])

#Q3 -d
library(pROC)

rocCurveDT <- roc(actual.dt,prediction.dt, direction = ">", levels = c("1", "0"))
rocCurveRF <- roc(actual.rf,prediction.rf, direction = ">", levels = c("1", "0"))

#plot the chart 
plot(rocCurveDT, col = 'red',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveRF, col = 'blue',main = "ROC Chart")

auc(rocCurveDT)
auc(rocCurveRF)



#Q4 -a
car$CarInsurancePred<-ifelse(predict(model.rf,car)>=0.6,'yes','no')
car$profit<-ifelse(car$CarInsurance==1 & car$CarInsurancePred == 'yes','100','0')
precntsCustomers<-length(car$CarInsurancePred[car$profit == 100 & car$CarInsurancePred == 'yes'])/length(car$CarInsurancePred)



